# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Nobuhiro Iwamatsu <iwamatsu@nigauri.org>, 2023
# Nick Schermer <nick@xfce.org>, 2023
# Xfce Bot <transifex@xfce.org>, 2023
# UTUMI Hirosi <utuhiro78@yahoo.co.jp>, 2023
# Masato HASHIMOTO <cabezon.hashimoto@gmail.com>, 2023
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-05-30 00:48+0200\n"
"PO-Revision-Date: 2023-02-20 11:54+0000\n"
"Last-Translator: Masato HASHIMOTO <cabezon.hashimoto@gmail.com>, 2023\n"
"Language-Team: Japanese (https://app.transifex.com/xfce/teams/16840/ja/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ja\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../data/interfaces/xfpm-settings.ui.h:1 ../settings/xfpm-settings.c:649
#: ../settings/xfpm-settings.c:664 ../settings/xfpm-settings.c:691
#: ../settings/xfpm-settings.c:1659
msgid "Never"
msgstr "しない"

#: ../data/interfaces/xfpm-settings.ui.h:2
msgid "When the screensaver is activated"
msgstr "スクリーンセーバーが起動したとき"

#: ../data/interfaces/xfpm-settings.ui.h:3
msgid "When the screensaver is deactivated"
msgstr "スクリーンセーバーから復帰したとき"

#: ../data/interfaces/xfpm-settings.ui.h:4
msgid "Nothing"
msgstr "何もしない"

#: ../data/interfaces/xfpm-settings.ui.h:5
#: ../settings/xfce4-power-manager-settings.desktop.in.h:1
#: ../src/xfpm-power.c:357 ../src/xfpm-power.c:633 ../src/xfpm-power.c:675
#: ../src/xfpm-power.c:838 ../src/xfpm-power.c:859 ../src/xfpm-backlight.c:166
#: ../src/xfpm-backlight.c:174 ../src/xfpm-battery.c:190
#: ../src/xfpm-kbd-backlight.c:118 ../src/xfce4-power-manager.desktop.in.h:1
msgid "Power Manager"
msgstr "電源管理"

#: ../data/interfaces/xfpm-settings.ui.h:6
#: ../panel-plugins/power-manager-plugin/xfce/xfce-power-manager-plugin.c:160
msgid "_Help"
msgstr "ヘルプ(_H)"

#: ../data/interfaces/xfpm-settings.ui.h:7
#: ../panel-plugins/power-manager-plugin/xfce/xfce-power-manager-plugin.c:161
msgid "_Close"
msgstr "閉じる(_C)"

#: ../data/interfaces/xfpm-settings.ui.h:8
msgid "When power button is pressed:"
msgstr "電源ボタンが押されたとき:"

#: ../data/interfaces/xfpm-settings.ui.h:9
msgid "When sleep button is pressed:"
msgstr "スリープボタンが押されたとき:"

#: ../data/interfaces/xfpm-settings.ui.h:10
msgid "When hibernate button is pressed:"
msgstr "ハイバネートボタンが押されたとき:"

#: ../data/interfaces/xfpm-settings.ui.h:11
msgid "When battery button is pressed:"
msgstr "バッテリーボタンが押されたとき:"

#: ../data/interfaces/xfpm-settings.ui.h:12
msgid "Exponential"
msgstr "急激"

#: ../data/interfaces/xfpm-settings.ui.h:13
msgid "Brightness step count:"
msgstr "輝度のステップ数:"

#: ../data/interfaces/xfpm-settings.ui.h:14
msgid "Handle display brightness _keys"
msgstr "ディスプレイの輝度キーを扱う(_K)"

#: ../data/interfaces/xfpm-settings.ui.h:15
msgid "<b>Buttons</b>"
msgstr "<b>ボタン</b>"

#: ../data/interfaces/xfpm-settings.ui.h:16
msgid "Status notifications"
msgstr "状態通知"

#: ../data/interfaces/xfpm-settings.ui.h:17
msgid "System tray icon"
msgstr "システムトレイアイコン"

#: ../data/interfaces/xfpm-settings.ui.h:18
msgid "<b>Appearance</b>"
msgstr "<b>外観</b>"

#: ../data/interfaces/xfpm-settings.ui.h:19
msgid "General"
msgstr "一般"

#: ../data/interfaces/xfpm-settings.ui.h:20
msgid "When inactive for"
msgstr "スリープするまでの時間"

#: ../data/interfaces/xfpm-settings.ui.h:21
msgid "System sleep mode:"
msgstr "システムのスリープモード:"

#: ../data/interfaces/xfpm-settings.ui.h:22
msgid "<b>System power saving</b>"
msgstr "<b>システムの省電力</b>"

#: ../data/interfaces/xfpm-settings.ui.h:23
msgid "<b>Laptop Lid</b>"
msgstr "<b>ラップトップの蓋</b>"

#: ../data/interfaces/xfpm-settings.ui.h:24
msgid "When laptop lid is closed:"
msgstr "ラップトップの蓋が閉じられたとき:"

#: ../data/interfaces/xfpm-settings.ui.h:25
msgid "On battery"
msgstr "バッテリ駆動"

#: ../data/interfaces/xfpm-settings.ui.h:26 ../common/xfpm-power-common.c:416
msgid "Plugged in"
msgstr "AC 接続"

#: ../data/interfaces/xfpm-settings.ui.h:27
msgid "Critical battery power level:"
msgstr "バッテリ電源の限界レベル:"

#: ../data/interfaces/xfpm-settings.ui.h:29 ../settings/xfpm-settings.c:699
#, no-c-format
msgid "%"
msgstr "%"

#: ../data/interfaces/xfpm-settings.ui.h:30
msgid "On critical battery power:"
msgstr "バッテリ電源の利用限界のとき:"

#: ../data/interfaces/xfpm-settings.ui.h:31
msgid "<b>Critical power</b>"
msgstr "<b>電源の限界</b>"

#: ../data/interfaces/xfpm-settings.ui.h:32
msgid "Lock screen when system is going to sleep"
msgstr "スリープ状態へ遷移中は画面をロックする"

#: ../data/interfaces/xfpm-settings.ui.h:33
msgid "<b>Security</b>"
msgstr "<b>セキュリティ</b>"

#: ../data/interfaces/xfpm-settings.ui.h:34
msgid "System"
msgstr "システム"

#: ../data/interfaces/xfpm-settings.ui.h:35
msgid "<b>Display power management</b>"
msgstr "<b>ディスプレイ電源管理</b>"

#: ../data/interfaces/xfpm-settings.ui.h:36
msgid ""
"Let the power manager handle display power management (DPMS) instead of X11."
msgstr "電源管理は X11 に代わってディスプレイ電源管理 (DPMS) を制御します。"

#: ../data/interfaces/xfpm-settings.ui.h:37
msgid "Blank after"
msgstr "ブランク画面にするまでの時間"

#: ../data/interfaces/xfpm-settings.ui.h:38
msgid "Put to sleep after"
msgstr "スリープするまでの時間"

#: ../data/interfaces/xfpm-settings.ui.h:39
msgid "Switch off after"
msgstr "電源を切断するまでの時間"

#: ../data/interfaces/xfpm-settings.ui.h:40
msgid "On inactivity reduce to"
msgstr "未使用時の輝度低下"

#: ../data/interfaces/xfpm-settings.ui.h:41
msgid "Reduce after"
msgstr "輝度を下げるまでの時間"

#: ../data/interfaces/xfpm-settings.ui.h:42
msgid "<b>Brightness reduction</b>"
msgstr "<b>輝度低下</b>"

#: ../data/interfaces/xfpm-settings.ui.h:43
msgid "Display"
msgstr "表示"

#: ../data/interfaces/xfpm-settings.ui.h:44
msgid "Automatically lock the session:"
msgstr "自動的にセッションをロック:"

#: ../data/interfaces/xfpm-settings.ui.h:45
msgid "Delay locking after screensaver for"
msgstr "スクリーンセーバー起動後ロックするまでの時間"

#: ../data/interfaces/xfpm-settings.ui.h:46
msgid "<b>Light Locker</b>"
msgstr "<b>Light Locker</b>"

#: ../data/interfaces/xfpm-settings.ui.h:47
msgid "Security"
msgstr "セキュリティ"

#: ../settings/xfpm-settings.c:652
msgid "One minute"
msgstr "1 分"

#: ../settings/xfpm-settings.c:654 ../settings/xfpm-settings.c:666
#: ../settings/xfpm-settings.c:677 ../settings/xfpm-settings.c:681
#: ../settings/xfpm-settings.c:1668
msgid "minutes"
msgstr "分"

#: ../settings/xfpm-settings.c:668 ../settings/xfpm-settings.c:675
#: ../settings/xfpm-settings.c:676 ../settings/xfpm-settings.c:677
msgid "One hour"
msgstr "1 時間"

#: ../settings/xfpm-settings.c:676 ../settings/xfpm-settings.c:680
msgid "one minute"
msgstr "1 分"

#: ../settings/xfpm-settings.c:679 ../settings/xfpm-settings.c:680
#: ../settings/xfpm-settings.c:681
msgid "hours"
msgstr "時間"

#: ../settings/xfpm-settings.c:693 ../settings/xfpm-settings.c:1661
msgid "seconds"
msgstr "秒"

#: ../settings/xfpm-settings.c:902 ../settings/xfpm-settings.c:981
#: ../settings/xfpm-settings.c:1043 ../settings/xfpm-settings.c:1139
#: ../settings/xfpm-settings.c:1231 ../settings/xfpm-settings.c:1350
#: ../settings/xfpm-settings.c:1408 ../settings/xfpm-settings.c:1460
#: ../settings/xfpm-settings.c:1511 ../src/xfpm-power.c:700
msgid "Suspend"
msgstr "サスペンド"

#: ../settings/xfpm-settings.c:906 ../settings/xfpm-settings.c:1143
msgid "Suspend operation not permitted"
msgstr "サスペンド操作が許可されていません"

#: ../settings/xfpm-settings.c:910 ../settings/xfpm-settings.c:1147
msgid "Suspend operation not supported"
msgstr "サスペンド操作はサポートされていません。"

#: ../settings/xfpm-settings.c:916 ../settings/xfpm-settings.c:987
#: ../settings/xfpm-settings.c:1049 ../settings/xfpm-settings.c:1153
#: ../settings/xfpm-settings.c:1237 ../settings/xfpm-settings.c:1356
#: ../settings/xfpm-settings.c:1414 ../settings/xfpm-settings.c:1466
#: ../settings/xfpm-settings.c:1517 ../src/xfpm-power.c:689
msgid "Hibernate"
msgstr "ハイバネート"

#: ../settings/xfpm-settings.c:920 ../settings/xfpm-settings.c:1157
msgid "Hibernate operation not permitted"
msgstr "ハイバネート操作が許可されていません"

#: ../settings/xfpm-settings.c:924 ../settings/xfpm-settings.c:1161
msgid "Hibernate operation not supported"
msgstr "ハイバネート操作はサポートされていません。"

#: ../settings/xfpm-settings.c:954 ../settings/xfpm-settings.c:1191
#: ../settings/xfpm-settings.c:1617 ../settings/xfpm-settings.c:1744
msgid "Hibernate and suspend operations not supported"
msgstr "ハイバネートおよびサスペンド操作はサポートされていません。"

#: ../settings/xfpm-settings.c:959 ../settings/xfpm-settings.c:1196
#: ../settings/xfpm-settings.c:1622 ../settings/xfpm-settings.c:1749
msgid "Hibernate and suspend operations not permitted"
msgstr "ハイバネートおよびサスペンド操作が許可されていません"

#: ../settings/xfpm-settings.c:976 ../settings/xfpm-settings.c:1345
#: ../settings/xfpm-settings.c:1403 ../settings/xfpm-settings.c:1455
#: ../settings/xfpm-settings.c:1506
msgid "Do nothing"
msgstr "何もしない"

#: ../settings/xfpm-settings.c:993 ../settings/xfpm-settings.c:1362
#: ../src/xfpm-power.c:711
msgid "Shutdown"
msgstr "シャットダウン"

#: ../settings/xfpm-settings.c:997 ../settings/xfpm-settings.c:1366
#: ../settings/xfpm-settings.c:1418 ../settings/xfpm-settings.c:1470
#: ../settings/xfpm-settings.c:1521
msgid "Ask"
msgstr "問い合わせる"

#: ../settings/xfpm-settings.c:1038 ../settings/xfpm-settings.c:1226
msgid "Switch off display"
msgstr "ディスプレイの電源を切る"

#: ../settings/xfpm-settings.c:1053 ../settings/xfpm-settings.c:1241
msgid "Lock screen"
msgstr "画面をロック"

#: ../settings/xfpm-settings.c:1550
msgid "Number of brightness steps available using keys"
msgstr "キーを使用するときの輝度ステップの数"

#: ../settings/xfpm-settings.c:1590
msgid "When all the power sources of the computer reach this charge level"
msgstr "コンピューターのすべての電源の充電レベルがこの値に達したときが対象となります"

#: ../settings/xfpm-settings.c:1666 ../common/xfpm-power-common.c:166
msgid "minute"
msgid_plural "minutes"
msgstr[0] "分"

#: ../settings/xfpm-settings.c:2033
msgid "Device"
msgstr "デバイス"

#: ../settings/xfpm-settings.c:2055
msgid "Type"
msgstr "タイプ"

#: ../settings/xfpm-settings.c:2060
msgid "PowerSupply"
msgstr "電源"

#: ../settings/xfpm-settings.c:2061 ../src/xfpm-main.c:77
msgid "True"
msgstr "あり"

#: ../settings/xfpm-settings.c:2061 ../src/xfpm-main.c:77
msgid "False"
msgstr "なし"

#: ../settings/xfpm-settings.c:2068
msgid "Model"
msgstr "モデル"

#: ../settings/xfpm-settings.c:2071
msgid "Technology"
msgstr "テクノロジ"

#: ../settings/xfpm-settings.c:2078
msgid "Current charge"
msgstr "現在の充電率"

#. TRANSLATORS: Unit here is Watt hour
#: ../settings/xfpm-settings.c:2086 ../settings/xfpm-settings.c:2098
#: ../settings/xfpm-settings.c:2110
msgid "Wh"
msgstr "Wh"

#: ../settings/xfpm-settings.c:2088
msgid "Fully charged (design)"
msgstr "フル充電されています (設計値)"

#: ../settings/xfpm-settings.c:2101
msgid "Fully charged"
msgstr "フル充電されています"

#: ../settings/xfpm-settings.c:2112
msgid "Energy empty"
msgstr "最小エネルギー密度"

#. TRANSLATORS: Unit here is Volt
#: ../settings/xfpm-settings.c:2120
msgid "V"
msgstr "V"

#: ../settings/xfpm-settings.c:2122
msgid "Voltage"
msgstr "電圧"

#: ../settings/xfpm-settings.c:2129
msgid "Vendor"
msgstr "ベンダー"

#: ../settings/xfpm-settings.c:2134
msgid "Serial"
msgstr "シリアル番号"

#: ../settings/xfpm-settings.c:2413
msgid "Check your power manager installation"
msgstr "電源管理が正しくインストールされていることを確認してください"

#: ../settings/xfpm-settings.c:2506
msgid "Devices"
msgstr "デバイス"

#: ../settings/xfpm-settings-app.c:74
msgid "Settings manager socket"
msgstr "設定マネージャーソケット"

#: ../settings/xfpm-settings-app.c:74
msgid "SOCKET ID"
msgstr "SOCKET ID"

#: ../settings/xfpm-settings-app.c:75
msgid "Display a specific device by UpDevice object path"
msgstr "UpDevice オブジェクトパスで指定されたデバイスを表示する"

#: ../settings/xfpm-settings-app.c:75
msgid "UpDevice object path"
msgstr "UpDevice object path"

#: ../settings/xfpm-settings-app.c:76 ../src/xfpm-main.c:276
msgid "Enable debugging"
msgstr "デバッグモードを有効にする"

#: ../settings/xfpm-settings-app.c:77
msgid "Display version information"
msgstr "バージョン情報を表示する"

#: ../settings/xfpm-settings-app.c:78
msgid "Cause xfce4-power-manager-settings to quit"
msgstr "xfce4-power-manager を終了する"

#: ../settings/xfpm-settings-app.c:174 ../settings/xfpm-settings-app.c:221
#: ../src/xfpm-main.c:448
msgid "Xfce Power Manager"
msgstr "Xfce 電源管理"

#: ../settings/xfpm-settings-app.c:176
msgid "Failed to connect to power manager"
msgstr "Xfce 電源管理への接続に失敗しました"

#: ../settings/xfpm-settings-app.c:190
msgid "Xfce4 Power Manager is not running, do you want to launch it now?"
msgstr "Xfce4 電源管理は動作していません。今起動しますか？"

#: ../settings/xfpm-settings-app.c:223
msgid "Failed to load power manager configuration, using defaults"
msgstr "電源管理設定の読み込みに失敗しました。デフォルト値を使用します。"

#: ../settings/xfpm-settings-app.c:356
#, c-format
msgid "This is %s version %s, running on Xfce %s.\n"
msgstr "%s バージョン %s, Xfce %s で実行されています。\n"

#: ../settings/xfpm-settings-app.c:358
#, c-format
msgid "Built with GTK+ %d.%d.%d, linked with GTK+ %d.%d.%d."
msgstr "Gtk+ %d.%d.%d でビルド、Gtk+ %d.%d.%d で実行しています。"

#: ../settings/xfce4-power-manager-settings.desktop.in.h:2
msgid "Settings for the Xfce Power Manager"
msgstr "Xfce 電源管理を設定します"

#: ../settings/xfce4-power-manager-settings.desktop.in.h:3
msgid ""
"settings;preferences;buttons;sleep;hibernate;battery;suspend;shutdown;brightness;laptop"
" lid;lock screen;plugged in;saving;critical;"
msgstr ""
"設定;環境設定;ボタン;スリープ;休止状態;バッテリー;一時停止;シャットダウン;明るさ;ノートパソコンの蓋;ロック画面;プラグイン;保存;重要;"

#: ../common/xfpm-common.c:120
msgid "translator-credits"
msgstr ""
"Daichi Kawahata <daichi@xfce.org>\n"
"Nobuhiro Iwamatsu <iwamatsu@nigauri.org>\n"
"Masato Hashimoto <hashimo@xfce.org>"

#: ../common/xfpm-power-common.c:45 ../common/xfpm-power-common.c:68
msgid "Battery"
msgstr "バッテリ"

#: ../common/xfpm-power-common.c:47
msgid "Uninterruptible Power Supply"
msgstr "無停電電源装置 (UPS)"

#: ../common/xfpm-power-common.c:49
msgid "Line power"
msgstr "AC 電源"

#: ../common/xfpm-power-common.c:51
msgid "Mouse"
msgstr "マウス"

#: ../common/xfpm-power-common.c:53
msgid "Keyboard"
msgstr "キーボード"

#: ../common/xfpm-power-common.c:55
msgid "Monitor"
msgstr "モニター"

#: ../common/xfpm-power-common.c:57
msgid "PDA"
msgstr "PDA"

#: ../common/xfpm-power-common.c:59
msgid "Phone"
msgstr "電話"

#: ../common/xfpm-power-common.c:61
msgid "Tablet"
msgstr "タブレット"

#: ../common/xfpm-power-common.c:63 ../common/xfpm-power-common.c:299
msgid "Computer"
msgstr "コンピューター"

#: ../common/xfpm-power-common.c:65 ../common/xfpm-power-common.c:81
#: ../common/xfpm-power-common.c:96
msgid "Unknown"
msgstr "不明"

#: ../common/xfpm-power-common.c:83
msgid "Lithium ion"
msgstr "リチウムイオン"

#: ../common/xfpm-power-common.c:85
msgid "Lithium polymer"
msgstr "リチウムポリマー"

#: ../common/xfpm-power-common.c:87
msgid "Lithium iron phosphate"
msgstr "リン酸鉄リチウム"

#: ../common/xfpm-power-common.c:89
msgid "Lead acid"
msgstr "鉛酸"

#: ../common/xfpm-power-common.c:91
msgid "Nickel cadmium"
msgstr "ニッケルカドミウム"

#: ../common/xfpm-power-common.c:93
msgid "Nickel metal hydride"
msgstr "ニッケル水素"

#: ../common/xfpm-power-common.c:141
msgid "Unknown time"
msgstr "不明"

#: ../common/xfpm-power-common.c:147
#, c-format
msgid "%i minute"
msgid_plural "%i minutes"
msgstr[0] "%i 分"

#: ../common/xfpm-power-common.c:158
#, c-format
msgid "%i hour"
msgid_plural "%i hours"
msgstr[0] "%i 時間"

#. TRANSLATOR: "%i %s %i %s" are "%i hours %i minutes"
#. * Swap order with "%2$s %2$i %1$s %1$i if needed
#: ../common/xfpm-power-common.c:164
#, c-format
msgid "%i %s %i %s"
msgstr "%i %s %i %s"

#: ../common/xfpm-power-common.c:165
msgid "hour"
msgid_plural "hours"
msgstr[0] "時間"

#: ../common/xfpm-power-common.c:339
#, c-format
msgid ""
"<b>%s %s</b>\n"
"Fully charged - %s remaining"
msgstr ""
"<b>%s %s</b>\n"
"フル充電済み - 残り %s"

#: ../common/xfpm-power-common.c:346
#, c-format
msgid ""
"<b>%s %s</b>\n"
"Fully charged"
msgstr ""
"<b>%s %s</b>\n"
"フル充電済み"

#: ../common/xfpm-power-common.c:355
#, c-format
msgid ""
"<b>%s %s</b>\n"
"%0.0f%% - %s until full"
msgstr ""
"<b>%s %s</b>\n"
"%0.0f%% - フル充電まで残り %s"

#: ../common/xfpm-power-common.c:363 ../common/xfpm-power-common.c:381
#, c-format
msgid ""
"<b>%s %s</b>\n"
"%0.0f%%"
msgstr ""
"<b>%s %s</b>\n"
"%0.0f%%"

#: ../common/xfpm-power-common.c:373
#, c-format
msgid ""
"<b>%s %s</b>\n"
"%0.0f%% - %s remaining"
msgstr ""
"<b>%s %s</b>\n"
"%0.0f%% - 残り %s"

#: ../common/xfpm-power-common.c:388
#, c-format
msgid ""
"<b>%s %s</b>\n"
"Waiting to discharge (%0.0f%%)"
msgstr ""
"<b>%s %s</b>\n"
"使用中です (%0.0f%%)"

#: ../common/xfpm-power-common.c:394
#, c-format
msgid ""
"<b>%s %s</b>\n"
"Waiting to charge (%0.0f%%)"
msgstr ""
"<b>%s %s</b>\n"
"充電待ちです (%0.0f%%)"

#: ../common/xfpm-power-common.c:400
#, c-format
msgid ""
"<b>%s %s</b>\n"
"is empty"
msgstr ""
"<b>%s %s</b>\n"
"空です"

#: ../common/xfpm-power-common.c:405
#, c-format
msgid ""
"<b>%s %s</b>\n"
"Current charge: %0.0f%%"
msgstr ""
"<b>%s %s</b>\n"
"現在の充電率: %0.0f%%"

#. On the 2nd line we want to know if the power cord is plugged
#. * in or not
#: ../common/xfpm-power-common.c:415
#, c-format
msgid ""
"<b>%s %s</b>\n"
"%s"
msgstr ""
"<b>%s %s</b>\n"
"%s"

#: ../common/xfpm-power-common.c:416
msgid "Not plugged in"
msgstr "AC 未接続"

#. Desktop pc with no battery, just display the vendor and model,
#. * which will probably just be Computer
#: ../common/xfpm-power-common.c:422
#, c-format
msgid "<b>%s %s</b>"
msgstr "<b>%s %s</b>"

#. unknown device state, just display the percentage
#: ../common/xfpm-power-common.c:427
#, c-format
msgid ""
"<b>%s %s</b>\n"
"Unknown state"
msgstr ""
"<b>%s %s</b>\n"
"状態が不明です"

#: ../src/xfpm-power.c:383
msgid ""
"An application is currently disabling the automatic sleep. Doing this action now may damage the working state of this application.\n"
"Are you sure you want to hibernate the system?"
msgstr ""
"アプリケーションは現在自動スリープが無効になっています。今ハイバネートを行うと、このアプリケーションの作業状態が破壊されるかもしれません。\n"
"本当にハイバネートしますか?"

#: ../src/xfpm-power.c:432
msgid ""
"None of the screen lock tools ran successfully, the screen will not be locked.\n"
"Do you still want to continue to suspend the system?"
msgstr ""
"スクリーンをロックするツールの起動に失敗したため、スクリーンはロックされません。\n"
"サスペンドを続行しますか?"

#: ../src/xfpm-power.c:597
msgid "Hibernate the system"
msgstr "システムをハイバネートします"

#: ../src/xfpm-power.c:608
msgid "Suspend the system"
msgstr "システムをサスペンドします"

#: ../src/xfpm-power.c:617
msgid "Shutdown the system"
msgstr "システムをシャットダウンします"

#: ../src/xfpm-power.c:628 ../src/xfpm-power.c:672
msgid "System is running on low power. Save your work to avoid losing data"
msgstr "システムは低電力下で動作しています。作業中のデータを失ってしまわないよう保存してください。"

#: ../src/xfpm-power.c:718
msgid "_Cancel"
msgstr "キャンセル(_C)"

#: ../src/xfpm-power.c:839
msgid "System is running on low power"
msgstr "システムは低電力下で動作しています"

#: ../src/xfpm-power.c:855
#, c-format
msgid ""
"Your %s charge level is low\n"
"Estimated time left %s"
msgstr ""
"%s の充電レベルが低くなっています\n"
"予想駆動時間は残り %s です"

#: ../src/xfpm-power.c:1648 ../src/xfpm-power.c:1696 ../src/xfpm-power.c:1728
#: ../src/xfpm-power.c:1758
msgid "Permission denied"
msgstr "アクセス拒否"

#: ../src/xfpm-power.c:1737 ../src/xfpm-power.c:1767
msgid "Suspend not supported"
msgstr "サスペンドはサポートされていません"

#. generate a human-readable summary for the notification
#: ../src/xfpm-backlight.c:160
#, c-format
msgid "Brightness: %.0f percent"
msgstr "輝度: %.0f %%"

#: ../src/xfpm-battery.c:109 ../src/xfpm-battery.c:160
#, c-format
msgid "Your %s is fully charged"
msgstr "%s はフル充電されています"

#: ../src/xfpm-battery.c:112 ../src/xfpm-battery.c:163
#, c-format
msgid "Your %s is charging"
msgstr "%s は充電中です"

#: ../src/xfpm-battery.c:122
#, c-format
msgid ""
"%s (%i%%)\n"
"%s until fully charged"
msgstr ""
"%s (%i%%)\n"
"フル充電まで %s です"

#: ../src/xfpm-battery.c:130 ../src/xfpm-battery.c:166
#, c-format
msgid "Your %s is discharging"
msgstr "%s を使用しています"

#: ../src/xfpm-battery.c:132
#, c-format
msgid "System is running on %s power"
msgstr "システムは %s 電源で動作しています"

#: ../src/xfpm-battery.c:142
#, c-format
msgid ""
"%s (%i%%)\n"
"Estimated time left is %s"
msgstr ""
"%s (%i%%)\n"
"推定残り時間は %s です"

#: ../src/xfpm-battery.c:148 ../src/xfpm-battery.c:169
#, c-format
msgid "Your %s is empty"
msgstr "%s は空です"

#. generate a human-readable summary for the notification
#: ../src/xfpm-kbd-backlight.c:116
#, c-format
msgid "Keyboard Brightness: %.0f percent"
msgstr "キーボードの輝度: %.0f %%"

#: ../src/xfpm-main.c:54
#, c-format
msgid ""
"\n"
"Xfce Power Manager %s\n"
"\n"
"Part of the Xfce Goodies Project\n"
"http://goodies.xfce.org\n"
"\n"
"Licensed under the GNU GPL.\n"
"\n"
msgstr ""
"\n"
"Xfce 電源管理 %s\n"
"\n"
"Part of the Xfce Goodies Project\n"
"http://goodies.xfce.org\n"
"\n"
"Licensed under the GNU GPL.\n"
"\n"

#: ../src/xfpm-main.c:112
#, c-format
msgid "With policykit support\n"
msgstr "PolicyKit サポートあり\n"

#: ../src/xfpm-main.c:114
#, c-format
msgid "Without policykit support\n"
msgstr "PolicyKit サポートなし\n"

#: ../src/xfpm-main.c:117
#, c-format
msgid "With network manager support\n"
msgstr "ネットワークマネージャーサポートあり\n"

#: ../src/xfpm-main.c:119
#, c-format
msgid "Without network manager support\n"
msgstr "ネットワークマネージャーサポートなし\n"

#: ../src/xfpm-main.c:134
msgid "Can suspend"
msgstr "サスペンドの可否"

#: ../src/xfpm-main.c:136
msgid "Can hibernate"
msgstr "ハイバネートの可否"

#: ../src/xfpm-main.c:138
msgid "Authorized to suspend"
msgstr "サスペンド権限"

#: ../src/xfpm-main.c:140
msgid "Authorized to hibernate"
msgstr "ハイバネート権限"

#: ../src/xfpm-main.c:142
msgid "Authorized to shutdown"
msgstr "シャットダウン権限"

#: ../src/xfpm-main.c:144
msgid "Has battery"
msgstr "バッテリの有無"

#: ../src/xfpm-main.c:146
msgid "Has brightness panel"
msgstr "輝度調整"

#: ../src/xfpm-main.c:148
msgid "Has power button"
msgstr "電源ボタンの有無"

#: ../src/xfpm-main.c:150
msgid "Has hibernate button"
msgstr "ハイバネートボタンの有無"

#: ../src/xfpm-main.c:152
msgid "Has sleep button"
msgstr "スリープボタンの有無"

#: ../src/xfpm-main.c:154
msgid "Has battery button"
msgstr "バッテリーボタンの有無"

#: ../src/xfpm-main.c:156
msgid "Has LID"
msgstr "蓋の有無"

#: ../src/xfpm-main.c:275
msgid "Daemonize"
msgstr "デーモン化"

#: ../src/xfpm-main.c:277
msgid "Dump all information"
msgstr "全情報を表示する"

#: ../src/xfpm-main.c:278
msgid "Restart the running instance of Xfce power manager"
msgstr "動作中の Xfce 電源管理インスタンスを再起動する"

#: ../src/xfpm-main.c:279
msgid "Show the configuration dialog"
msgstr "設定ダイアログを表示します"

#: ../src/xfpm-main.c:280
msgid "Quit any running xfce power manager"
msgstr "動作中の Xfce 電源管理を終了する"

#: ../src/xfpm-main.c:281
msgid "Version information"
msgstr "バージョン情報を表示する"

#: ../src/xfpm-main.c:300
#, c-format
msgid "Failed to parse arguments: %s\n"
msgstr "引数の解析に失敗しました: %s\n"

#: ../src/xfpm-main.c:329
#, c-format
msgid "Type '%s --help' for usage."
msgstr "'%s --help' と入力すると使用方法が表示されます。"

#: ../src/xfpm-main.c:350
msgid "Unable to get connection to the message bus session"
msgstr "メッセージバスセッションに接続できませんでした"

#: ../src/xfpm-main.c:358 ../src/xfpm-main.c:407
#, c-format
msgid "Xfce power manager is not running"
msgstr "Xfce 電源管理は動作していません"

#: ../src/xfpm-main.c:449
msgid "Another power manager is already running"
msgstr "ほかの電源管理がすでに動作しています"

#: ../src/xfpm-main.c:453
#, c-format
msgid "Xfce power manager is already running"
msgstr "Xfce 電源管理はすでに動作しています"

#: ../src/xfpm-inhibit.c:396
msgid "Invalid arguments"
msgstr "不正な引数です"

#: ../src/xfpm-inhibit.c:430
msgid "Invalid cookie"
msgstr "不正な Cookie です"

#: ../src/xfpm-manager.c:455
msgid ""
"None of the screen lock tools ran successfully, the screen will not be "
"locked."
msgstr "スクリーンをロックするツールの起動に失敗したため、スクリーンはロックされません。"

#: ../src/xfce4-power-manager.desktop.in.h:2
msgid "Power management for the Xfce desktop"
msgstr "Xfce デスクトップ用電源管理"

#. Odds are this is a desktop without any batteries attached
#: ../panel-plugins/power-manager-plugin/power-manager-button.c:294
msgid "Display battery levels for attached devices"
msgstr "接続されたデバイスのバッテリレベルを表示します"

#. Translators this is to display which app is inhibiting
#. * power in the plugin menu. Example:
#. * VLC is currently inhibiting power management
#: ../panel-plugins/power-manager-plugin/power-manager-button.c:1455
#, c-format
msgid "%s is currently inhibiting power management"
msgstr "現在の %s が電源管理を抑止しています。"

#: ../panel-plugins/power-manager-plugin/power-manager-button.c:1750
msgid "<b>Display brightness</b>"
msgstr "<b>ディスプレイの輝度</b>"

#: ../panel-plugins/power-manager-plugin/power-manager-button.c:1777
#: ../panel-plugins/power-manager-plugin/power-manager-button.c:1788
msgid "Presentation _mode"
msgstr "プレゼンテーションモード(_M)"

#. Power manager settings
#: ../panel-plugins/power-manager-plugin/power-manager-button.c:1801
msgid "_Settings..."
msgstr "設定(_S)..."

#: ../panel-plugins/power-manager-plugin/xfce/xfce-power-manager-plugin.c:78
#, c-format
msgid "Unable to open the following url: %s"
msgstr "以下の URL を開くことができません: %s"

#: ../panel-plugins/power-manager-plugin/xfce/xfce-power-manager-plugin.c:149
msgid "None"
msgstr "無し"

#: ../panel-plugins/power-manager-plugin/xfce/xfce-power-manager-plugin.c:149
msgid "Percentage"
msgstr "パーセンテージ"

#: ../panel-plugins/power-manager-plugin/xfce/xfce-power-manager-plugin.c:149
msgid "Remaining time"
msgstr "残り時間"

#: ../panel-plugins/power-manager-plugin/xfce/xfce-power-manager-plugin.c:149
msgid "Percentage and remaining time"
msgstr "パーセンテージと残り時間"

#. create the dialog
#: ../panel-plugins/power-manager-plugin/xfce/xfce-power-manager-plugin.c:157
msgid "Power Manager Plugin Settings"
msgstr "パワーマネージャープラグイン設定"

#. show-panel-label setting
#: ../panel-plugins/power-manager-plugin/xfce/xfce-power-manager-plugin.c:180
msgid "Show label:"
msgstr "ラベル表示:"

#: ../panel-plugins/power-manager-plugin/xfce/xfce-power-manager-plugin.c:213
msgid "Show 'Presentation mode' indicator:"
msgstr "「プレゼンテーションモード」インジケーターを表示:"

#: ../panel-plugins/power-manager-plugin/xfce/power-manager-plugin.desktop.in.in.h:1
msgid "Power Manager Plugin"
msgstr "電源管理プラグイン"

#: ../panel-plugins/power-manager-plugin/xfce/power-manager-plugin.desktop.in.in.h:2
msgid ""
"Display the battery levels of your devices and control the brightness of "
"your display"
msgstr "デバイスのバッテリレベルを表示し、ディスプレイの輝度を制御します"

#: ../data/appdata/xfce4-power-manager.appdata.xml.in.h:1
msgid ""
"Xfce power manager manages the power sources on the computer and the devices"
" that can be controlled to reduce their power consumption (such as LCD "
"brightness level, monitor sleep, CPU frequency scaling)."
msgstr ""
"Xfce 電源管理はコンピューターおよび消費電力を制御可能なデバイス (LCD の輝度レベル、モニターのスリープ、CPU の動作周波数など) "
"の電源を管理します。"

#: ../data/appdata/xfce4-power-manager.appdata.xml.in.h:2
msgid ""
"In addition, Xfce power manager provides a set of freedesktop-compliant DBus"
" interfaces to inform other applications about current power level so that "
"they can adjust their power consumption, and it provides the inhibit "
"interface which allows applications to prevent automatic sleep actions via "
"the power manager; as an example, the operating system’s package manager "
"should make use of this interface while it is performing update operations."
msgstr ""
"さらに、Xfce 電源管理はほかのアプリケーションに現在の電源レベルなどを提供するための freedesktop.org 準拠の DBus "
"インターフェイスも用意しています。これを利用してアプリケーションから消費電力の調整が行えます。ほかに、電源制御が自動で行うスリープの一時的な抑止もできます。例えば、オペレーティングシステムのパッケージ管理がアップデート処理を行っている間などにこの機能を使用するといいでしょう。"

#: ../data/appdata/xfce4-power-manager.appdata.xml.in.h:3
msgid ""
"Xfce power manager also provides a plugin for the Xfce and LXDE panels to "
"control LCD brightness levels and to monitor battery and device charge "
"levels."
msgstr ""
"また、Xfce パネル用と LXDE パネル用のプラグインも 2 種類提供しています。一つは LCD "
"の輝度レベルを調整するもの、もう一つはバッテリやデバイスの充電レベルを監視するものです。"

#: ../data/appdata/xfce4-power-manager.appdata.xml.in.h:4
msgid ""
"This development release mostly fixes bugs and introduces better support for"
" icon-themes by reducing the device icons and using standard names for them."
" It also features updated translations. The panel plugin has been renamed to"
" Power Manager Plugin."
msgstr ""
"この開発版リリースでは、ほとんどのバグが修正され、デバイスアイコンを減らして標準名を使用することによりアイコンテーマのサポートを改善しました。パネルプラグインは名称を「電源管理プラグイン」に変更し、翻訳を更新しました。"

#: ../data/appdata/xfce4-power-manager.appdata.xml.in.h:5
msgid ""
"This development release fixes bugs with suspend and hibernate. It also "
"improves the panel-plugin, incorporating the functionality of the (now "
"dropped) brightness panel-plugin. A new popup notification was added to "
"account for keyboard brightness changes and the Power Manager now also "
"controls the X11 blank times."
msgstr ""
"この開発版リリースでは、サスペンドおよびハイバネートに関するバグが修正されました。他に、パネルプラグインを改善し、輝度プラグインと機能統合されました "
"(輝度プラグインは廃止)。新しくキーボードの輝度が変更されたときの通知機能が追加され、電源管理で X11 ブランク時間を制御できるようになりました。"

#: ../data/appdata/xfce4-power-manager.appdata.xml.in.h:6
msgid ""
"This development release introduces a lot of new features, among them "
"suspending/hibernation without systemd and UPower>=0.99. It allows for "
"individual control of systemd-inhibition, a new panel plugin to monitor "
"battery and device charge levels replaces the trayicon. The settings dialog "
"has been completely restructured for better oversight and many open bugs "
"have been fixed and translations have been updated."
msgstr ""
"この開発版リリースでは多くの新機能を導入しました。systemd および UPower>=0.99 なしでのサスペンド/ハイバネートでは "
"systemd-inhibit "
"の個別制御が行え、トレイアイコンはバッテリインジケータープラグインに置き換えられました。設定ダイアログは完全に書き直され、多くのバグが修正されました。"

#: ../data/appdata/xfce4-power-manager.appdata.xml.in.h:7
msgid ""
"This stable release fixes compilation problems, a memory leak and a few "
"other minor bugs. Furthermore it provides updated translations."
msgstr "この安定版リリースではコンパイルの問題、メモリリーク、およびいくつかの小さなバグが修正されました。"
